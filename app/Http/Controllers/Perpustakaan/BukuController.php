<?php

namespace App\Http\Controllers\Perpustakaan;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\Models\Perpustakaan\Buku;
use App\Http\Resources\BukuResource;
use App\Http\Resources\BukuCollection;
use App\Http\Requests\BukuRequest;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Buku::paginate(10);
        // return BukuResource::collection($books);
        return new BukuCollection($books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BukuRequest $request)
    {
        // $request->validate([
        //     'kode' => ['required', 'max:20'],
        //     'judul' => ['required', 'max:100'],
        //     'pengarang' => ['required', 'max:50'],
        //     'terbit' => ['required', 'max:20']
        // ]);

        $books = new Buku($this->bukuStore());
        $books->save();
        return $books;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Buku $buku)
    {
        return new BukuResource($buku);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BukuRequest $request, Buku $buku)
    {
        $buku->update($this->bukuStore());
        return new BukuResource($buku);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buku $buku)
    {
        $buku->delete();

        return response()->json(['message'=>'Buku sudah berhasil dihapus'], 200);
    }

    public function bukuStore(){
        return [
            'kode' => request('kode'),
            'judul' => request('judul'),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => request('terbit'),
        ];
    }
}
