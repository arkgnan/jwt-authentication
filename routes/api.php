<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Auth')->group(function(){
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::get('user', 'UserController');

// route untuk tugas peminjaman buku oleh mahasiswa
Route::namespace('Perpustakaan')->group(function(){
    Route::middleware('auth:api')->group(function(){
        Route::post('input-buku', 'BukuController@store');
        Route::patch('edit-buku/{buku}', 'BukuController@update');
        Route::delete('hapus-buku/{buku}', 'BukuController@destroy');

        Route::post('pinjam-buku', 'PinjamController@store');
        Route::patch('kembaliin-buku/{pinjam}', 'PinjamController@update')->middleware('admin');
    });
    Route::get('buku/{buku}', 'BukuController@show');
    Route::get('buku', 'BukuController@index');
});

// Route::patch('kembaliin', 'Perpustakaan\PinjamController@update')->middleware(['login', 'admin']);