<?php

namespace App\models\Perpustakaan;

use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    protected $fillable = ['nim', 'buku_id', 'tanggal_pinjam', 'batas_akhir', 'pengembalian', 'is_ontime'];

    protected $hidden = ['created_at', 'updated_at'];

}
