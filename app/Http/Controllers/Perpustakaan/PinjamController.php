<?php

namespace App\Http\Controllers\Perpustakaan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Perpustakaan\Pinjam;

class PinjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'buku_id' => ['required']
        ]);
        
        $user = auth()->user()->mahasiswa()->first()->nim;
        $current_date = Carbon::now()->format('Y-m-d');
        $tanggal_kembali = Carbon::now()->addDays(1)->format('Y-m-d');
        
        $pinjam = Pinjam::create([
            'nim'=> $user,
            'buku_id'=> request('buku_id'),
            'tanggal_pinjam'=> $current_date,
            'batas_akhir'=> $tanggal_kembali
        ]);

        return $pinjam;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pinjam $pinjam)
    {
        $max_balik = $pinjam->first()->batas_akhir.' 23:59:59';
        $current_date = Carbon::now();
        if ($current_date->gt(Carbon::parse($max_balik))){
            $ontime = 0;
        } else {
            $ontime = 1;
        }

        $pinjam->update([
            'pengembalian' => $current_date->format('Y-m-d'),
            'is_ontime' => $ontime
        ]);
        // dd($kembalikan);
        return $pinjam;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
