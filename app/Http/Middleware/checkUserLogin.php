<?php

namespace App\Http\Middleware;

use Closure;

class checkUserLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->user() ){
            return $next($request);
        } else {
            return response()->json(['message'=> 'Login dan masukkan token terlebih dahulu'], 401);
        }
    }
}
