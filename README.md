tugas ke 3

membuat API dengan jwt-auth

contoh kasus peminjaman buku oleh mahasiswa

endpoint api:
register : api/register (form-data : nama, username, email, password, handphone, whatsapp, fakultas, jurusan, role_access:optional->default mahasiswa)  
login : api/login (form-data : email, password)  
logout : api/logout

input buku : api/input-buku (form-data : kode, judul, pengarang, terbit)  
edit buku : api/edit-buku/{kode_buku} (form-data : kode, judul, pengarang, terbit)  
hapus buku : api/hapus-buku/{kode_buku}

pinjam buku : api/kembaliin-buku/{id_pinjam} : api/pinjam-buku (form-data : buku_id)

**hanya bisa diakses admin**  
mengembalikan buku (hanya bisa diakses admin) : api/kembaliin-buku/{id_pinjam}

**notes**  
semua end point buku harus menggunakan bearer token (login untuk mendapatkannya)
