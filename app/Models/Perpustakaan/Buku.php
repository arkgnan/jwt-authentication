<?php

namespace App\Models\Perpustakaan;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $fillable = ['kode', 'judul', 'pengarang', 'tahun_terbit'];

    protected $hidden = ['created_at', 'updated_at'];

    public function getRouteKeyName()
    {
        return 'kode';
    }
    public function pinjam()
    {
        return $this->hasMany(Pinjam::class);
    }
}
